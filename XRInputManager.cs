﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

[HelpURL("https://docs.google.com/spreadsheets/d/1xHfMj0L5-tPupcPpSJaxxw9bjSZiSyMOJe3si0pNt6s/edit#gid=0")]
public class XRInputManager : MonoBehaviour
{
    public static XRInputManager Instance { get; private set; }

    public enum Hand
    {
        Right = 0,
        Left = 1
    }

    public enum Button
    {
        Trigger,
        Grip,
        Thumbstick,
        ThumbstickUp,
        ThumbstickDown,
        ThumbstickLeft,
        ThumbstickRight,
        Primary,
        Secondary,
        MenuButton
    }

    enum ButtonState
    {
        WasPressed,
        Pressed,
        WasReleased,
        Released
    }

    [SerializeField] private float PressTriggerThreshold = 0.8f;
    [SerializeField] private float ReleaseTriggerThreshold = 0.8f;
    [SerializeField] private float PressGripThreshold = 0.8f;
    [SerializeField] private float ReleaseGripThreshold = 0.8f;
    [SerializeField] private float PressThumbstickThreshold = 0.4f;
    [SerializeField] private float ReleaseThumbstickThreshold = 0.4f;

    private List<InputDevice> _allDevices = new List<InputDevice>();
    private HandVariables[] _hand = new HandVariables[2] { new HandVariables(), new HandVariables()}; // 0 = RightHand 1 = LeftHand
    private Vector3 tempVel = Vector3.zero;
    private int VelocityFramesLimit = 10;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        UpdateButtonStates(Hand.Right);
        UpdateButtonStates(Hand.Left);
        UpdatePreviousVariables();
        InputDevices.GetDevices(_allDevices);
        ReadAndCacheInputs();
        CalculateButtonsState(Hand.Right);
        CalculateButtonsState(Hand.Left);
    }

    void UpdatePreviousVariables()
    {
        int _rightHandIndex = (int)Hand.Right;
        _hand[_rightHandIndex].prevTriggerValue = _hand[_rightHandIndex].triggerValue;
        _hand[_rightHandIndex].prevGripValue = _hand[_rightHandIndex].gripValue;
        _hand[_rightHandIndex].prevPrimaryButtonPress = _hand[_rightHandIndex].primaryButtonPress;
        _hand[_rightHandIndex].prevSecondaryButtonPress = _hand[_rightHandIndex].secondaryButtonPress;
        _hand[_rightHandIndex].prevThumbstickPressed = _hand[_rightHandIndex].thumbstickPressed;
        _hand[_rightHandIndex].prevThumbstickValue = _hand[_rightHandIndex].thumbstickValue;
        _hand[_rightHandIndex].prevMenuButton = _hand[_rightHandIndex].menuButton;

        int _leftHandIndex = (int)Hand.Left;
        _hand[_leftHandIndex].prevTriggerValue = _hand[_leftHandIndex].triggerValue;
        _hand[_leftHandIndex].prevGripValue = _hand[_leftHandIndex].gripValue;
        _hand[_leftHandIndex].prevPrimaryButtonPress = _hand[_leftHandIndex].primaryButtonPress;
        _hand[_leftHandIndex].prevSecondaryButtonPress = _hand[_leftHandIndex].secondaryButtonPress;
        _hand[_leftHandIndex].prevThumbstickPressed = _hand[_leftHandIndex].thumbstickPressed;
        _hand[_leftHandIndex].prevThumbstickValue = _hand[_leftHandIndex].thumbstickValue;
        _hand[_leftHandIndex].prevMenuButton = _hand[_leftHandIndex].menuButton;
    }

    void ReadAndCacheInputs()
    {
        for (int i = 0; i < _allDevices.Count; i++)
        {
            if (_allDevices[i].role == InputDeviceRole.RightHanded)
            {
                CacheInputs(_allDevices[i], Hand.Right);
            }
            else if (_allDevices[i].role == InputDeviceRole.LeftHanded)
            {
                CacheInputs(_allDevices[i], Hand.Left);
            }
        }
    }

    void CacheInputs(InputDevice inputDevice,Hand hand)
    {
        int indexCasted = (int)hand;
        inputDevice.TryGetFeatureValue(CommonUsages.devicePosition, out _hand[indexCasted].position);
        inputDevice.TryGetFeatureValue(CommonUsages.deviceRotation, out _hand[indexCasted].rotation);
        inputDevice.TryGetFeatureValue(CommonUsages.trigger, out _hand[indexCasted].triggerValue);
        inputDevice.TryGetFeatureValue(CommonUsages.grip, out _hand[indexCasted].gripValue);
        inputDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out _hand[indexCasted].thumbstickValue);
        inputDevice.TryGetFeatureValue(CommonUsages.primaryButton, out _hand[indexCasted].primaryButtonPress);
        inputDevice.TryGetFeatureValue(CommonUsages.secondaryButton, out _hand[indexCasted].secondaryButtonPress);
        inputDevice.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out _hand[indexCasted].thumbstickPressed);
        inputDevice.TryGetFeatureValue(CommonUsages.primaryTouch, out _hand[indexCasted].touchPrimaryButton);
        inputDevice.TryGetFeatureValue(CommonUsages.secondaryTouch, out _hand[indexCasted].touchSecondaryButton);
        inputDevice.TryGetFeatureValue(CommonUsages.triggerButton, out _hand[indexCasted].touchTrigger);
        inputDevice.TryGetFeatureValue(CommonUsages.primary2DAxisTouch, out _hand[indexCasted].touchThumbstick);
        inputDevice.TryGetFeatureValue(CommonUsages.deviceVelocity, out tempVel);
        _hand[indexCasted].listHandVelocity.Add(tempVel);
        if (_hand[indexCasted].listHandVelocity.Count > VelocityFramesLimit)
        {
            _hand[indexCasted].listHandVelocity.RemoveAt(0);
        }
        inputDevice.TryGetFeatureValue(CommonUsages.deviceAngularVelocity, out _hand[indexCasted].angularVelocity);
        inputDevice.TryGetFeatureValue(CommonUsages.batteryLevel, out _hand[indexCasted].batteryLevel);
        inputDevice.TryGetFeatureValue(CommonUsages.isTracked, out _hand[indexCasted].isTracked);

    }

    private void CalculateButtonsState(Hand hand)
    {
        int indexCasted = (int)hand;
        //Trigger
        SetButtonsState(_hand[indexCasted].triggerValue, _hand[indexCasted].prevTriggerValue, ref _hand[indexCasted].triggerState, PressTriggerThreshold, ReleaseTriggerThreshold);
        //Grip
        SetButtonsState(_hand[indexCasted].gripValue, _hand[indexCasted].prevGripValue, ref _hand[indexCasted].gripState, PressGripThreshold, ReleaseGripThreshold);
        //ThumbstickUp
        SetButtonsState(_hand[indexCasted].thumbstickValue.y, _hand[indexCasted].prevThumbstickValue.y, ref _hand[indexCasted].thumbstickUp, PressThumbstickThreshold, ReleaseThumbstickThreshold);
        //ThumbstickDown
        SetButtonsState(-_hand[indexCasted].thumbstickValue.y, -_hand[indexCasted].prevThumbstickValue.y, ref _hand[indexCasted].thumbstickDown, PressThumbstickThreshold, ReleaseThumbstickThreshold);
        //ThumbstickRight
        SetButtonsState(_hand[indexCasted].thumbstickValue.x, _hand[indexCasted].prevThumbstickValue.x, ref _hand[indexCasted].thumbstickRight, PressThumbstickThreshold, ReleaseThumbstickThreshold);
        //ThumbstickLeft
        SetButtonsState(-_hand[indexCasted].thumbstickValue.x, -_hand[indexCasted].prevThumbstickValue.x, ref _hand[indexCasted].thumbstickLeft, PressThumbstickThreshold, ReleaseThumbstickThreshold);
    }

    private void SetButtonsState(float buttonValue, float prevButtonValue, ref ButtonState curButtonState, float pressThreshold, float releaseThreshold)
    {
        if (buttonValue >= pressThreshold && prevButtonValue < pressThreshold)
        {
            if (curButtonState == ButtonState.Released || curButtonState == ButtonState.WasReleased)
            {
                curButtonState = ButtonState.WasPressed;
            }
        }
        else if (buttonValue < releaseThreshold && prevButtonValue >= releaseThreshold)
        {
            if (curButtonState == ButtonState.Pressed || curButtonState == ButtonState.WasPressed)
            {
                curButtonState = ButtonState.WasReleased;
            }
        }
    }

    private void UpdateButtonStates(Hand hand)
    {
        int indexCasted = (int)hand;
        //Trigger
        ExtrapolateButtonState(ref _hand[indexCasted].triggerState);
        //Grip
        ExtrapolateButtonState(ref _hand[indexCasted].gripState);
        //ThumbstickUp
        ExtrapolateButtonState(ref _hand[indexCasted].thumbstickUp);
        //ThumbstickDown
        ExtrapolateButtonState(ref _hand[indexCasted].thumbstickDown);
        //ThumbstickRight
        ExtrapolateButtonState(ref _hand[indexCasted].thumbstickRight);
        //ThumbstickLeft
        ExtrapolateButtonState(ref _hand[indexCasted].thumbstickLeft);
    }

    private void ExtrapolateButtonState(ref ButtonState ButtonState)
    {
        if (ButtonState == ButtonState.WasPressed)
        {
            ButtonState = ButtonState.Pressed;
        }
        else if (ButtonState == ButtonState.WasReleased)
        {
            ButtonState = ButtonState.Released;
        }
    }

    public void SetTrackingOrigin(TrackingSpaceType trackingSpaceType)
    {
        XRDevice.SetTrackingSpaceType(trackingSpaceType);
    }

    public Vector3 GetControllerPosition(Hand hand)
    {
        return _hand[(int)hand].position;
    }

    public Quaternion GetControllerRotation(Hand hand)
    {
        return _hand[(int)hand].rotation;
    }

    public Vector2 GetThumbstick(Hand hand)
    {
        return _hand[(int)hand].thumbstickValue;
    }

    public float GetTrigger(Hand hand)
    {
        return _hand[(int)hand].triggerValue;
    }

    public float GetGrip(Hand hand)
    {
        return _hand[(int)hand].gripValue;
    }

    public bool GetTouch(Hand hand, Button button)
    {
        switch (button) {
            case Button.Primary:
                return _hand[(int)hand].touchPrimaryButton;
            case Button.Secondary:
                return _hand[(int)hand].touchSecondaryButton;
            case Button.Thumbstick:
                return _hand[(int)hand].touchThumbstick;
            case Button.Trigger:
                return _hand[(int)hand].touchTrigger;
        }
        return false;
    }

    public bool Get(Hand hand, Button button)
    {
        return GetButtonsState(hand, button, ButtonState.Pressed);
    }

    public bool GetDown(Hand hand, Button button)
    {
        return GetButtonsState(hand, button, ButtonState.WasPressed);
    }

    public bool GetUp(Hand hand, Button button)
    {
        return GetButtonsState(hand, button, ButtonState.WasReleased);
    }

    private bool GetButtonsState(Hand hand, Button button, ButtonState targetState)
    {
        switch (button)
        {
            case Button.Trigger:
                return _hand[(int)hand].triggerState == targetState;
            case Button.Grip:
                return _hand[(int)hand].gripState == targetState;
            case Button.Primary:
                return GetBoolButtonsState(targetState, _hand[(int)hand].primaryButtonPress, _hand[(int)hand].prevPrimaryButtonPress);
            case Button.Secondary:
                return GetBoolButtonsState(targetState, _hand[(int)hand].secondaryButtonPress, _hand[(int)hand].prevSecondaryButtonPress);
            case Button.Thumbstick:
                return GetBoolButtonsState(targetState, _hand[(int)hand].thumbstickPressed, _hand[(int)hand].prevThumbstickPressed);
            case Button.ThumbstickUp:
                return _hand[(int)hand].thumbstickUp == targetState;
            case Button.ThumbstickDown:
                return _hand[(int)hand].thumbstickDown == targetState;
            case Button.ThumbstickRight:
                return _hand[(int)hand].thumbstickRight == targetState;
            case Button.ThumbstickLeft:
                return _hand[(int)hand].thumbstickLeft == targetState;
            case Button.MenuButton:
                return GetBoolButtonsState(targetState, _hand[(int)hand].menuButton, _hand[(int)hand].prevMenuButton);
        }
        return false;
    }

    private bool GetBoolButtonsState(ButtonState targetState, bool button,bool prevButton)
    {
        switch (targetState)
        {
            case ButtonState.Pressed:
                return button;
            case ButtonState.WasPressed:
                return (button && !prevButton);
            case ButtonState.WasReleased:
                return (!button && prevButton);
        }
        return false;
    }

    public Vector3 GetVelocity(Hand hand)
    {
        if (_hand[(int)hand].listHandVelocity.Count >0)
        {
            return _hand[(int)hand].listHandVelocity[_hand[(int)hand].listHandVelocity.Count];
        }
        return Vector3.zero;
    }

    public Vector3 GetVelocity(Hand hand, int Frames)
    {
        Frames = Mathf.Clamp(Frames, 1, VelocityFramesLimit);
        Vector3 outputVel;
        Vector3 AcuumVel = Vector3.zero;
        if (_hand[(int)hand].listHandVelocity.Count >= Frames)
        {
            for (int i = _hand[(int)hand].listHandVelocity.Count - 1; i > (_hand[(int)hand].listHandVelocity.Count - 1 - Frames); i--)
            {
                AcuumVel += _hand[(int)hand].listHandVelocity[i];
            }
            outputVel = AcuumVel / Frames;
        }
        else
        {
            for (int i = 0; i < _hand[(int)hand].listHandVelocity.Count; i++)
            {
                AcuumVel += _hand[(int)hand].listHandVelocity[i];
            }
            outputVel = AcuumVel / _hand[(int)hand].listHandVelocity.Count;
        }
        return outputVel;
    }

    public Vector3 GetAngularVelocity(Hand hand)
    {
        return _hand[(int)hand].angularVelocity;
    }

    public float GetBattery(Hand hand)
    {
        return _hand[(int)hand].batteryLevel;
    }

    public bool IsTracked(Hand hand)
    {
        return _hand[(int)hand].isTracked;
    }

    public bool IsUserPresent()
    {
        if(XRDevice.userPresence == UserPresenceState.NotPresent)
        {
            return (false);
        }
        return (XRDevice.userPresence == UserPresenceState.Present);
    }

    [System.Serializable]
    private class HandVariables
    {
        public List<Vector3> listHandVelocity = new List<Vector3>();
        public Vector3 angularVelocity = Vector3.zero;
        public Vector3 position = Vector3.zero;
        public Quaternion rotation = Quaternion.identity;
        public float triggerValue = 0f;
        public float gripValue = 0f;
        public float prevTriggerValue = 0f;
        public float prevGripValue = 0f;
        public ButtonState triggerState = ButtonState.Released;
        public ButtonState gripState = ButtonState.Released;
        public Vector2 thumbstickValue = Vector2.zero;
        public Vector2 prevThumbstickValue = Vector2.zero;
        public ButtonState thumbstickUp = ButtonState.Released;
        public ButtonState thumbstickDown = ButtonState.Released;
        public ButtonState thumbstickLeft = ButtonState.Released;
        public ButtonState thumbstickRight = ButtonState.Released;
        public bool thumbstickPressed = false;
        public bool prevThumbstickPressed = false;
        public bool primaryButtonPress = false;
        public bool prevPrimaryButtonPress = false;
        public bool secondaryButtonPress = false;
        public bool prevSecondaryButtonPress = false;
        public bool touchPrimaryButton = false;
        public bool touchSecondaryButton = false;
        public bool touchThumbstick = false;
        public bool touchTrigger = false;
        public float batteryLevel = 0f;
        public bool isTracked = false;
        public bool menuButton = false;
        public bool prevMenuButton = false;
    }
}